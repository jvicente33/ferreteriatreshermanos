/*


 Aqui se codifica todas aquellas funciones 
    que ayuden para cosas pequeñas


 */
package Vista;

import Modelo.Conexion;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class Funciones {
    
    private static int estado;
    
    Conexion c=new Conexion();
    
    public Funciones(int est){
        this.estado=est;
    }
    
    public Funciones(){
        
    }
    
    public int getEstado(){
        return this.estado;
    }
    public void setEstado(int est){
        this.estado=est;
    }
    
    
    //Intercambio de ventanas, para abrir otra y cerrar la actual
    //La que deseas abrir se pasa el .fxml
    //La que deseas cerrar, se paga el id del AnchorPane
    public void intercambio(String abrir, AnchorPane cerrar) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(abrir));
           Parent root1 = (Parent) fxmlLoader.load();
           Stage stage = new Stage();
           stage.setScene(new Scene(root1));
           stage.initStyle(StageStyle.UNDECORATED);
           stage.show();
           
           
           Stage stagee = (Stage) cerrar.getScene().getWindow();
           stagee.close();
    }
    
    public void cerrarSolamente(AnchorPane cerrar){
        Stage stagee = (Stage) cerrar.getScene().getWindow();
        stagee.close();
    }
    
    public void abrirSolamente(String abrir) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(abrir));
           Parent root = (Parent) fxmlLoader.load();
           Stage stage = new Stage();
           stage.setScene(new Scene(root));  
           stage.initStyle(StageStyle.UNDECORATED);
           stage.show();
           
    }
    
    public void nomina(FileWriter fichero){
        
        int cantidad=c.getCantNomina();
        
        PrintWriter pw = null;
        try
        {
            pw = new PrintWriter(fichero);

            for (int i = 1; i < cantidad+1; i++){
                ArrayList<String> datos=c.getNomina(i);
                String nombre=c.getNombre(datos.get(0));
                pw.println(nombre+"  --------------------------------");
                pw.println("Cedula --> " + datos.get(0));
                pw.println("Cargo --> " + datos.get(1));
                pw.println("Sueldo Neto --> " + datos.get(2));
                pw.println("IVSS --> " + datos.get(3));
                pw.println("LPH --> " + datos.get(4));
                pw.println("Paro Forzoso --> " + datos.get(5));
                int total_pagar=(int) (Double.parseDouble(datos.get(2))-Double.parseDouble(datos.get(3))-Double.parseDouble(datos.get(4))-Double.parseDouble(datos.get(5)));
                pw.println("Total --> " + Integer.toString(total_pagar));
                pw.println("----------------------------------------");
                pw.println("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
        
    }
    
    public void factura(FileWriter fichero, ArrayList<String> empresa, ArrayList<String> venta, ArrayList<String> articulos, ArrayList<String> forma){
        
        PrintWriter pw = null;
        try
        {
            pw = new PrintWriter(fichero);

                pw.println("        SENIAT      ");
                pw.println(empresa.get(1));
                pw.println(empresa.get(0));
                pw.println(empresa.get(2));
                pw.println(empresa.get(3));
                pw.println("");
                pw.println("Cliente: " + venta.get(2));
                pw.println("Cedula|RIF: " + venta.get(3));
                pw.println("");
                pw.println("        FACTURA      ");
                pw.println("Nro. Factura: " + venta.get(0));
                pw.println("Fecha: " + venta.get(1));
                pw.println("-------------------");
                for(int i=0;i<articulos.size();i++){
                    pw.println(articulos.get(i));
                }
                pw.println("-------------------");
                pw.println("Subtotal: " + venta.get(4));
                pw.println("-------------------");
                int sub=(int) (Double.parseDouble(venta.get(4))*(0.12));
                pw.println("IVA (12%): "+sub);
                pw.println("-------------------");
                pw.println("TOTAL --> "+venta.get(5));
                pw.println("");
                for(int i=0;i<forma.size();i++){
                    pw.println(forma.get(i));
                }
    
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
        
    }
    
    public void ingresosMensual(FileWriter fichero, int mes){
        
        PrintWriter pw = null;
        try
        {
            pw = new PrintWriter(fichero);

                ArrayList<String> datos=c.ingresosMensual(mes);
                pw.println("Mes -- > " + mes + "---------------------------");
                for (int i=0;i<datos.size()-1;i++){
                    pw.println(datos.get(i));
                }
                pw.println("---------------------------------");
                int tam=datos.size();
                pw.println("TOTAL DEL MES --> " + datos.get(tam-1));
                
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
        
    }
    
    public void reporteZ(FileWriter fichero, String fecha){
        
        PrintWriter pw = null;
        try
        {
            pw = new PrintWriter(fichero);

                ArrayList<String> datos=c.reporteZ(fecha);
                pw.println("Fecha -- > " + fecha + "----------------------------------");
                for (int i=0;i<datos.size()-1;i++){
                    pw.println(datos.get(i));
                }
                pw.println("-----------------------------------------");
                int tam=datos.size();
                pw.println("TOTAL DEL DIA --> " + datos.get(tam-1));
                
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
        
    }
    
}
