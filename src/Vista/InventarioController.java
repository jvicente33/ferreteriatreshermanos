
package Vista;

import Controlador.Inventario;
import Modelo.Conexion;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import static com.sun.java.accessibility.util.AWTEventMonitor.addWindowListener;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.swing.JOptionPane;


public class InventarioController implements Initializable {
    
    @FXML
    TableView<Inventario> tabla_inventario= new TableView<Inventario>();

    @FXML
    private JFXDrawer draww;

    @FXML
    private JFXButton btn_buscar;

    @FXML
    private JFXButton btn_eliminar;

    @FXML
    private JFXHamburger menu;
    
    @FXML
    TableColumn<Inventario, String> id;
    
    @FXML
    TableColumn<Inventario, String> descripcion;
    
    @FXML
    TableColumn<Inventario, String> grupo;
    
    @FXML
    TableColumn<Inventario, String> proveedor;
    
    @FXML
    TableColumn<Inventario, String> cant;
    
    @FXML
    TableColumn<Inventario, String> precio;

    @FXML
    private JFXButton btn_modificar;
    
    @FXML
    private JFXButton btn_salir;
    
    @FXML
    private JFXButton btn_agregar;
    
    @FXML
    private AnchorPane ventana_inventario;
    
    final ObservableList<Inventario> data = FXCollections.observableArrayList(); 
    
    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
            
        draww.setVisible(false);

        //------CODIGO PARA ABRIR EL MENU
        try {
            AnchorPane box = FXMLLoader.load(getClass().getResource("subpanel.fxml"));
            draww.setSidePane(box);
        } catch (IOException ex) {
            Logger.getLogger(PanelController.class.getName()).log(Level.SEVERE, null, ex);
        }

        HamburgerBackArrowBasicTransition transition = new HamburgerBackArrowBasicTransition(menu);
        transition.setRate(-1);
        menu.addEventHandler(MouseEvent.MOUSE_PRESSED,(e)->{
            draww.setVisible(true);
            transition.setRate(transition.getRate()*-1);
            transition.play();

            if(draww.isShown())
            {
                draww.close();
                draww.setVisible(false);
            }else{
                draww.open();
            }
        });
        //FIN DEL CODIGO PARA ABRIR EL MENU

        //Codigo para Visualizar la tabla del inventario

        id.setCellValueFactory(new PropertyValueFactory<Inventario, String>("Id"));
        descripcion.setCellValueFactory(new PropertyValueFactory<Inventario, String>("Descripcion"));
        grupo.setCellValueFactory(new PropertyValueFactory<Inventario, String>("Grupo"));
        proveedor.setCellValueFactory(new PropertyValueFactory<Inventario, String>("Proveedor"));
        cant.setCellValueFactory(new PropertyValueFactory<Inventario, String>("Cantidad"));
        precio.setCellValueFactory(new PropertyValueFactory<Inventario, String>("Precio"));

        tabla_inventario.setItems(data);

        //Insertar datos en tabla
        ingresarDatos();

        //FIN DE LA TABLA

        //Para insertar imagen en los botones
        Image image = new Image(getClass().getResourceAsStream("icon\\buscar.png"));
        btn_buscar.setGraphic(new ImageView(image));

        Image image2 = new Image(getClass().getResourceAsStream("icon\\agregar.png"));
        btn_agregar.setGraphic(new ImageView(image2));

        Image image3 = new Image(getClass().getResourceAsStream("icon\\modificar.png"));
        btn_modificar.setGraphic(new ImageView(image3));

        Image image4 = new Image(getClass().getResourceAsStream("icon\\eliminar.png"));
        btn_eliminar.setGraphic(new ImageView(image4));
        
        Image image5 = new Image(getClass().getResourceAsStream("icon\\salir.png"));
        btn_salir.setGraphic(new ImageView(image5));
        
        //TIEMPO PARA ACTUALIZAR TABLA
        
        Timer timer = new Timer();
        TimerTask timerTask;
        
        timerTask = new TimerTask()
        {
            public void run()
            {
                actualizarTabla();
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 1000);

        //----

    }    
   

    
    @FXML
    public String extraer(){
        String dato=null;
        try {
            TablePosition pos = tabla_inventario.getSelectionModel().getSelectedCells().get(0);
            int row = pos.getRow();
            dato=(String) id.getCellObservableValue(row).getValue();
        } catch ( Exception e ) {
            System.out.println("ERROR AL SELECCIONAR ITEM");
        }
        return dato;
    }
    
    @FXML
    public void ingresarDatos(){
        
        ArrayList<String> aux=c.mostrarInventario();
        
        for (int i=0; i<aux.size(); i++){
            
            StringTokenizer st = new StringTokenizer(aux.get(i),"|");
            ArrayList<String> temp=new ArrayList<String>();
            
            while (st.hasMoreTokens()) {  
                temp.add(st.nextToken());
            }  
            
            data.add(new Inventario(temp.get(0),temp.get(1),temp.get(2),temp.get(3),temp.get(4),temp.get(5)));
        }
        
    }
    
    @FXML
    public void agregar() throws IOException{
        
        f.abrirSolamente("inventario_agregar.fxml");
        
    }
    
    @FXML
    public void modificar() throws IOException{
        
        f.abrirSolamente("inventario_modificar.fxml");
        
    }
    
    @FXML
    public void eliminar(){
        
 
        String id=extraer();
        if(id!=null){
            int ax = JOptionPane.showConfirmDialog(null, "¿Desea Eliminar este Dato?");
            if(ax == JOptionPane.YES_OPTION){
                c.eliminar(id, "articulos");
                Funciones funciones=new Funciones(1);
                JOptionPane.showMessageDialog(null, "Dato Eliminado.");
            }
        }else{
            JOptionPane.showMessageDialog(null, "Por favor seleccione una fila...");
        }
        
        
    }
    
    @FXML
    public void consultar(){
        
        ArrayList<String> datos;
        String id=extraer();
        if(id!=null){
            datos=c.buscarCodInventario(Integer.parseInt(id));
            JOptionPane.showMessageDialog(null, "ID: " + datos.get(0) + "\nCod. Interno: " + datos.get(1) + "\nCod. Externo: " + datos.get(2) + "\nDescripcion: " + datos.get(3) + "\nMarca: " + datos.get(4) + "\nModelo: " + datos.get(5) + "\nID Grupo: " + datos.get(6) + "\nID Proveedor: " + datos.get(7) + "\nCamtidad: " + datos.get(8) + "\nPrecio Base: " + datos.get(9) + "\nFecha Entrada: " + datos.get(10) + "\nUltima Modificacion: " + datos.get(11) + "\nEstatus: " + datos.get(12) + "\nVendidos: " + datos.get(13) + "\nDevueltos: " + datos.get(14) + "\nID Empleado: " + datos.get(15));
        }else{
            JOptionPane.showMessageDialog(null, "Por favor seleccione una fila...");
        }
        
        actualizarTabla();
    }
    
    @FXML
    public void actualizarTabla(){

        int estado=f.getEstado();
        if(estado==1){
            //Borrar filas
            for ( int i = 0; i<tabla_inventario.getItems().size(); i++) {
            tabla_inventario.getItems().clear();
            }
            
            id.setCellValueFactory(new PropertyValueFactory<Inventario, String>("Id"));
            descripcion.setCellValueFactory(new PropertyValueFactory<Inventario, String>("Descripcion"));
            grupo.setCellValueFactory(new PropertyValueFactory<Inventario, String>("Grupo"));
            proveedor.setCellValueFactory(new PropertyValueFactory<Inventario, String>("Proveedor"));
            cant.setCellValueFactory(new PropertyValueFactory<Inventario, String>("Cantidad"));
            precio.setCellValueFactory(new PropertyValueFactory<Inventario, String>("Precio"));
        
            tabla_inventario.setItems(data);
            
            ingresarDatos();
            Funciones funciones=new Funciones(0);
        }
        
    }
    
    @FXML
    public void salir(){
        
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea salir?");
        if(ax == JOptionPane.YES_OPTION){
            System.exit(0);
            /*timerTask.cancel();
            timer.cancel();*/
        }

    }
    
}
