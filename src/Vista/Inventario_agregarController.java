package Vista;

import Modelo.Conexion;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;


public class Inventario_agregarController implements Initializable {
    
    @FXML
    private JFXTextField descripcion;

    @FXML
    private JFXTextField marca;

    @FXML
    private JFXComboBox<String> grupo;

    @FXML
    private JFXTextField cod_externo;

    @FXML
    private JFXComboBox<String> proveedor;

    @FXML
    private JFXTextField cantidad;

    @FXML
    private JFXTextField modelo;

    @FXML
    private JFXTextField precio_base;

    @FXML
    private JFXButton btn_aceptar;
    
    @FXML
    private JFXButton btn_salir;
    
    @FXML
    private JFXTextField cod_interno;
    
    @FXML
    private AnchorPane ventana_agregar_inventario;
    
    Date fecha = new Date();
    int año=fecha.getYear()+1900;
    String dia=fecha.getDate()+"-"+fecha.getMonth()+"-"+año;
    
    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        //------COLOCAR EN NEGRITA EL PROMPT TEXT
        
        cod_externo.setStyle("-fx-prompt-text-fill: #424242;"); 
        cod_interno.setStyle("-fx-prompt-text-fill: #424242;"); 
        descripcion.setStyle("-fx-prompt-text-fill: #424242;"); 
        marca.setStyle("-fx-prompt-text-fill: #424242;"); 
        modelo.setStyle("-fx-prompt-text-fill: #424242;"); 
        grupo.setStyle("-fx-prompt-text-fill: #424242;"); 
        proveedor.setStyle("-fx-prompt-text-fill: #424242;");
        cantidad.setStyle("-fx-prompt-text-fill: #424242;"); 
        precio_base.setStyle("-fx-prompt-text-fill: #424242;"); 
        
        //------INICIALIZAR EL COMBO BOX
        
        ArrayList<String> aux=c.mostrarGrupos();
        ArrayList<String> aux2=c.mostrarProveedores();
        
        for (int i=0; i<aux.size();i++){
            grupo.getItems().add(aux.get(i));
        }
        
        for (int i=0; i<aux2.size();i++){
            proveedor.getItems().add(aux2.get(i));
        }

        Image image5 = new Image(getClass().getResourceAsStream("icon\\salir.png"));
        btn_salir.setGraphic(new ImageView(image5));
        
    }    
    
    @FXML
    public void ingresarDatos(){
        
        String code, codi, des, mar, mod, grup, pro, cant, precio;
        code=cod_externo.getText();
        codi=cod_interno.getText();
        des=descripcion.getText();
        mar=marca.getText();
        mod=modelo.getText();
        grup=(String) grupo.getValue();
        pro=(String) proveedor.getValue();
        cant=cantidad.getText();
        precio=precio_base.getText();
        
        
        String[] vector=new String[16];
        vector[0]=Integer.toString(c.generarId("articulos"));
        vector[1]=code;
        vector[2]=codi;
        vector[3]=des;
        vector[4]=mar;
        vector[5]=mod;

        StringTokenizer st = new StringTokenizer(grup,"-");  
        StringTokenizer st2 = new StringTokenizer(pro,"-"); 
        int i=0;
        while (st.hasMoreTokens()) { 
            String a=st.nextToken();
           if(i==0){
               vector[6]=a;
           }
           i++;
        }  
        i=0;
        while (st2.hasMoreTokens()) {  
            String a=st2.nextToken();
         if(i==0){
               vector[7]=a;
           }
           i++; 
        }  
        
        vector[8]=cant;
        vector[9]=precio;
        vector[10]=this.dia;
        vector[11]=this.dia;
        vector[12]="ACTIVO";
        vector[13]="0";
        vector[14]="0";
        vector[15]="0";
        
        
        //System.out.println(code + "|" + codi + "|" + des + "|" + mar + "|" + mod + "|" + grup + "|" + pro + "|" + cant + "|" +precio);
        c.agregarInventario(vector);
        
        f.cerrarSolamente(ventana_agregar_inventario);
        
        Funciones funciones=new Funciones(1);
        
        JOptionPane.showMessageDialog(null, "Dato Agregado.");
    }
    
    @FXML
    public void salir(){
        
        f.cerrarSolamente(ventana_agregar_inventario);
        
    }
    
}
