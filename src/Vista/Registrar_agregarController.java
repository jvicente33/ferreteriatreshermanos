package Vista;

import Modelo.Conexion;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javax.swing.JOptionPane;


public class Registrar_agregarController implements Initializable {

    @FXML
    private AnchorPane ventana_agregar_registro;
    @FXML
    private GridPane grid_clientes;
    @FXML
    private JFXTextField nombre_cliente;
    @FXML
    private JFXTextField cedula_cliente;
    @FXML
    private JFXTextField direccion_cliente;
    @FXML
    private JFXTextField email_cliente;
    @FXML
    private JFXTextField telfl_cliente;
    @FXML
    private JFXTextField telfc_cliente;
    @FXML
    private JFXButton btn_aceptar;
    @FXML
    private JFXButton btn_salir;
    @FXML
    private GridPane grid_proveedores;
    @FXML
    private JFXTextField nombre_proveedor;
    @FXML
    private JFXTextField rif_proveedor;
    @FXML
    private JFXTextField direccion_proveedor;
    @FXML
    private JFXTextField email_proveedor;
    @FXML
    private JFXTextField telefono1_proveedor;
    @FXML
    private JFXTextField telefono2_proveedor;
    @FXML
    private GridPane grid_empleados;
    @FXML
    private JFXTextField ficha_empleado;
    @FXML
    private JFXTextField nombres_empleado;
    @FXML
    private JFXTextField apellidos_empleado;
    @FXML
    private JFXTextField cedula_empleado;
    @FXML
    private JFXTextField direccion_empleado;
    @FXML
    private DatePicker nacimiento_empleado;
    @FXML
    private JFXTextField telfl_empleado;
    @FXML
    private JFXTextField telfc_empleado;
    @FXML
    private JFXTextField email_empleado;
    @FXML
    private JFXComboBox<String> tipo_empleado;
    @FXML
    private JFXComboBox<String> tipo_registro;
    
    Date fecha = new Date();
    int año=fecha.getYear()+1900;
    String dia=fecha.getDate()+"-"+(fecha.getMonth()+1)+"-"+año;
    
    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       //Ingresar items
       ingresarItem();
       
       Image image5 = new Image(getClass().getResourceAsStream("icon\\salir.png"));
       btn_salir.setGraphic(new ImageView(image5));
    }    
    
    @FXML
    public void ingresarItem(){
        
        tipo_registro.getItems().add("Clientes");
        tipo_registro.getItems().add("Proveedores");
        tipo_registro.getItems().add("Empleados");
       
        
    }
    
    
    @FXML
    public void mostrarRegistro(){
        
        String a, b, c;
        a="Clientes";
        b="Proveedores";
        c="Empleados";
        String aux=(String) tipo_registro.getValue();

        int op=aux.compareTo(a);
        int op2=aux.compareTo(b);
        int op3=aux.compareTo(c);
        
        if(op==0){
            grid_clientes.setVisible(true);
        }
        if(op2==0){
            grid_proveedores.setVisible(true);
        }
        if(op3==0){
            grid_empleados.setVisible(true);
        }
        
    }
    
    @FXML
    public void registrarCliente(){
        
        ArrayList<String> cliente=new ArrayList<String>();
        cliente.add(Integer.toString(c.generarId("clientes")));
        cliente.add(nombre_cliente.getText());
        cliente.add(cedula_cliente.getText());
        cliente.add(direccion_cliente.getText());
        cliente.add(telfl_cliente.getText());
        cliente.add(telfc_cliente.getText());
        cliente.add(email_cliente.getText());
        cliente.add(dia);
        cliente.add(dia);
        cliente.add("0");
        cliente.add("");
        
        c.ingresarCliente(cliente);
        JOptionPane.showMessageDialog(null, "Cliente registrado con exito.");
    }
    
    @FXML
    public void registrarProveedor(){
        
        ArrayList<String> proveedor=new ArrayList<String>();
        proveedor.add(Integer.toString(c.generarId("proveedores")));
        proveedor.add(nombre_proveedor.getText());
        proveedor.add(rif_proveedor.getText());
        proveedor.add(telefono1_proveedor.getText());
        proveedor.add(telefono2_proveedor.getText());
        proveedor.add(email_proveedor.getText());
        proveedor.add(dia);
        proveedor.add(dia);
        proveedor.add("0");
        proveedor.add("");
        
        c.ingresarProveedores(proveedor);
        JOptionPane.showMessageDialog(null, "Proveedor registrado con exito.");
    }
    
    @FXML
    public void registrarEmpleado(){
        
        ArrayList<String> empleado=new ArrayList<String>();
        empleado.add(Integer.toString(c.generarId("empleados")));
        empleado.add(ficha_empleado.getText());
        empleado.add(nombres_empleado.getText());
        empleado.add(apellidos_empleado.getText());
        empleado.add(cedula_empleado.getText());
        empleado.add("");
        empleado.add(direccion_empleado.getText());
        
        LocalDate ingreso=nacimiento_empleado.getValue();
        DateTimeFormatter formater=DateTimeFormatter.ofPattern("dd-LLLL-yyyy");
        String ingresoAux=ingreso.format(formater);
        
        empleado.add(ingresoAux);
        empleado.add(telfl_empleado.getText());
        empleado.add(telfc_empleado.getText());
        empleado.add(email_empleado.getText());
        empleado.add("1");
        empleado.add(Integer.toString(c.generarId("empleados")));
        empleado.add(dia);
        empleado.add(dia);
        empleado.add("");
        
        c.ingresarEmpleados(empleado);
        JOptionPane.showMessageDialog(null, "Empleado registrado con exito.");
    }
    
    @FXML
    public void ingresarRegistro(){
        
        String a, b, c;
        a="Clientes";
        b="Proveedores";
        c="Empleados";
        String aux=(String) tipo_registro.getValue();

        int op=aux.compareTo(a);
        int op2=aux.compareTo(b);
        int op3=aux.compareTo(c);
        
        if(op==0){
            registrarCliente();
            f.cerrarSolamente(ventana_agregar_registro);
        }
        if(op2==0){
            registrarProveedor();
            f.cerrarSolamente(ventana_agregar_registro);
        }
        if(op3==0){
            registrarEmpleado();
            f.cerrarSolamente(ventana_agregar_registro);
        }
        
    }
    
    @FXML
    public void salir(){
        
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea salir?");
        if(ax == JOptionPane.YES_OPTION){
            f.cerrarSolamente(ventana_agregar_registro);
            /*timerTask.cancel();
            timer.cancel();*/
        }

    }
    
}
