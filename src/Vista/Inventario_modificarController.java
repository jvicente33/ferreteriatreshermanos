
package Vista;

import Modelo.Conexion;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javax.swing.JOptionPane;


public class Inventario_modificarController implements Initializable {

    @FXML
    private AnchorPane ventana_inventario_modificar;
    @FXML
    private JFXTextField cod_interno;
    @FXML
    private JFXTextField cod_externo;
    @FXML
    private JFXTextField descripcion;
    @FXML
    private JFXTextField marca;
    @FXML
    private JFXTextField modelo;
    @FXML
    private JFXComboBox<String> grupo;
    @FXML
    private JFXComboBox<String> proveedor;
    @FXML
    private JFXTextField cantidad;
    @FXML
    private JFXTextField precio_base;
    @FXML
    private JFXTextField id;
    @FXML
    private JFXTextField fecha_entrada;
    @FXML
    private JFXTextField fecha_ultima;
    @FXML
    private JFXTextField estatus;
    @FXML
    private JFXTextField vendidos;
    @FXML
    private JFXTextField devueltos;
    @FXML
    private JFXTextField id_empleado;
    @FXML
    private JFXTextField buscar;
    @FXML
    private JFXButton btn_modificar;
    @FXML
    private JFXButton btn_salir;
    @FXML
    private GridPane grid;

    
    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Image image5 = new Image(getClass().getResourceAsStream("icon\\salir.png"));
        btn_salir.setGraphic(new ImageView(image5));
       
    }    
    
    @FXML
    public void buscarCod(){
        
        int aux_id=Integer.parseInt(buscar.getText());
        ArrayList<String> datos;
        datos=c.buscarCodInventario(aux_id);
        
        buscar.setDisable(true);
        grid.setDisable(false);
        
        
        ArrayList<String> aux=c.mostrarGrupos();
        ArrayList<String> aux2=c.mostrarProveedores();
        
        for (int i=0; i<aux.size();i++){
            grupo.getItems().add(aux.get(i));
        }
        
        for (int i=0; i<aux2.size();i++){
            proveedor.getItems().add(aux2.get(i));
        }
        
        
        id.setText(datos.get(0));
        id.setDisable(true);
        
        cod_interno.setText(datos.get(1));
        cod_externo.setText(datos.get(2));
        descripcion.setText(datos.get(3));
        marca.setText(datos.get(4));
        modelo.setText(datos.get(5));
        
        String grup=null;
        for (int i=0;i<aux.size();i++){
            StringTokenizer st = new StringTokenizer(aux.get(i),"-");  
            int j=0;
            while (st.hasMoreTokens()) { 
                
                if(j==0){
                    int a=Integer.parseInt(st.nextToken());
                    int b=Integer.parseInt(datos.get(6));
                    if(a==b){
                        grup=aux.get(i);
                    }   
                }else{
                    st.nextToken();
                }
                j++;
            }  
        }
        grupo.getSelectionModel().select(grup);
        
        String prov=null;
        for (int i=0;i<aux2.size();i++){
            StringTokenizer st = new StringTokenizer(aux2.get(i),"-");  
            int j=0;
            while (st.hasMoreTokens()) { 
                
                if(j==0){
                    int a=Integer.parseInt(st.nextToken());
                    int b=Integer.parseInt(datos.get(7));
                    if(a==b){
                        prov=aux2.get(i);
                    }   
                }else{
                    st.nextToken();
                }
                j++;
            }  
        }
        proveedor.getSelectionModel().select(prov);
        
        
        cantidad.setText(datos.get(8));
        precio_base.setText(datos.get(9));
        fecha_entrada.setText(datos.get(10));
        fecha_ultima.setText(datos.get(11));
        estatus.setText(datos.get(12));
        vendidos.setText(datos.get(13));
        devueltos.setText(datos.get(14));
        id_empleado.setText(datos.get(15));
        id_empleado.setDisable(true);
        
        
        
    }
    
    @FXML
    public void modificar() throws SQLException{
        
        ArrayList<String> datos=new ArrayList<String>();
        datos.add(id.getText());
        datos.add(cod_interno.getText());
        datos.add(cod_externo.getText());
        datos.add(descripcion.getText());
        datos.add(marca.getText());
        datos.add(modelo.getText());
        
        StringTokenizer st = new StringTokenizer((String) grupo.getValue(),"-");  
        StringTokenizer st2 = new StringTokenizer((String) proveedor.getValue(),"-"); 
        int i=0;
        while (st.hasMoreTokens()) { 
            String a=st.nextToken();
           if(i==0){
               datos.add(a);
           }
           i++;
        }  
        i=0;
        while (st2.hasMoreTokens()) {  
            String a=st2.nextToken();
         if(i==0){
               datos.add(a);
           }
           i++; 
        }  
        
        datos.add(cantidad.getText());
        datos.add(precio_base.getText());
        datos.add(fecha_entrada.getText());
        datos.add(fecha_ultima.getText());
        datos.add(estatus.getText());
        datos.add(vendidos.getText());
        datos.add(devueltos.getText());
        datos.add(id_empleado.getText());
        
        c.modificarInventario(Integer.parseInt(datos.get(0)), datos);
        Funciones funciones=new Funciones(1);
        JOptionPane.showMessageDialog(null, "Dato Modificado.");
    }
    
    @FXML
    public void salir(){
        
        f.cerrarSolamente(ventana_inventario_modificar);
        
    }
    
}
