package Vista;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

public class PanelController implements Initializable {

    @FXML
    private AnchorPane ventana_panel;
    @FXML
    private JFXDrawer draww;
    @FXML
    private JFXHamburger menu;
   
    Funciones f= new Funciones();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        draww.setVisible(false);
        
        //CODIGO PARA ABRIR EL MENU
        try {
            AnchorPane box = FXMLLoader.load(getClass().getResource("subpanel.fxml"));
            draww.setSidePane(box);
        } catch (IOException ex) {
            Logger.getLogger(PanelController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        HamburgerBackArrowBasicTransition transition = new HamburgerBackArrowBasicTransition(menu);
        transition.setRate(-1);
        menu.addEventHandler(MouseEvent.MOUSE_PRESSED,(e)->{
            draww.setVisible(true);
            transition.setRate(transition.getRate()*-1);
            transition.play();
            
            if(draww.isShown())
            {
                draww.close();
                draww.setVisible(false);
            }else{
                draww.open();
            }
        });
        //FIN DEL CODIGO 
     
    }
    
    @FXML
    public void cerrar() throws IOException{
        f.cerrarSolamente(ventana_panel);
    }
    
}
