package Vista;

import Modelo.Conexion;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javax.swing.JOptionPane;


public class LoginController implements Initializable {

    @FXML
    private JFXTextField cedula;
    
    @FXML
    private JFXPasswordField clave;

    @FXML
    private AnchorPane ventana_login;
    
    @FXML
    private JFXButton ingresar;
    
    @FXML
    private JFXButton btn_salir;
    @FXML
    private VBox vobx;
    
    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        vobx.requestFocus();
        
        cedula.setStyle("-fx-prompt-text-fill: #2E2E2E;"); 
        clave.setStyle("-fx-prompt-text-fill: #2E2E2E;"); 
        ingresar.getStyleClass().add("button-raised");
        ventana_login.setStyle("-fx-background-color: white;"); 
        
        
        //Para insertar imagen en los botones
        Image image = new Image(getClass().getResourceAsStream("icon\\salir.png"));
        btn_salir.setGraphic(new ImageView(image));
         
        
    }    
    
    

    @FXML
    public void ingresarPanel() throws IOException{
        boolean op=validarDatos();
        
        if(op==true){
            f.intercambio("panel.fxml",ventana_login);
        }else{
            JOptionPane.showMessageDialog(null, "ERROR - Usuario o Contraseña Invalida");
            cedula.setText("");
            clave.setText("");
            cedula.requestFocus();
        }
    }
    
    @FXML 
    public boolean validarDatos(){
        
        String ced=cedula.getText();
        String cla=clave.getText();
        boolean op=c.login(ced, cla);
        return op;
        
    }
    
    @FXML
    public void salir(){
        System.exit(0);
    }

}
