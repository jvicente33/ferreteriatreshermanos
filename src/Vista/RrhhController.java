package Vista;

import Modelo.Conexion;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javax.swing.JOptionPane;


public class RrhhController implements Initializable {

    @FXML
    private AnchorPane ventana_rrhh;
    @FXML
    private JFXHamburger menu;
    @FXML
    private JFXDrawer draww;
    @FXML
    private JFXButton btn_salir;
    @FXML
    private GridPane grid;
    @FXML
    private JFXTextField nombre;
    @FXML
    private JFXTextField cedula;
    @FXML
    private JFXTextField sueldo;
    @FXML
    private JFXTextField cesta;
    @FXML
    private JFXTextField ivss;
    @FXML
    private JFXTextField pf;
    @FXML
    private JFXTextField lph;
    @FXML
    private JFXComboBox<String> fecha_pago;
    @FXML
    private JFXComboBox<String> contrato;
    @FXML
    private DatePicker fecha_ingreso;
    @FXML
    private JFXTextField cargo;
    @FXML
    private JFXButton btn_aceptar;
    @FXML
    private JFXButton btn_reset;
    
    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        fecha_pago.getItems().add("MENSUAL");
        fecha_pago.getItems().add("QUINCENAL");
        fecha_pago.getItems().add("SEMANAL");
        
        contrato.getItems().add("CONTRATADO");
        contrato.getItems().add("FIJO");
        
        
        Image image5 = new Image(getClass().getResourceAsStream("icon\\salir.png"));
        btn_salir.setGraphic(new ImageView(image5));
        
        nombre.setStyle("-fx-prompt-text-fill: #424242;"); 
        cedula.setStyle("-fx-prompt-text-fill: #424242;"); 
        sueldo.setStyle("-fx-prompt-text-fill: #424242;"); 
        cesta.setStyle("-fx-prompt-text-fill: #424242;"); 
        cargo.setStyle("-fx-prompt-text-fill: #424242;"); 
        ivss.setStyle("-fx-prompt-text-fill: #424242;"); 
        pf.setStyle("-fx-prompt-text-fill: #424242;"); 
        lph.setStyle("-fx-prompt-text-fill: #424242;"); 
        
        draww.setVisible(false);
        
         //------CODIGO PARA ABRIR EL MENU
        try {
            AnchorPane box = FXMLLoader.load(getClass().getResource("subpanel.fxml"));
            draww.setSidePane(box);
        } catch (IOException ex) {
            Logger.getLogger(PanelController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        HamburgerBackArrowBasicTransition transition = new HamburgerBackArrowBasicTransition(menu);
        transition.setRate(-1);
        menu.addEventHandler(MouseEvent.MOUSE_PRESSED,(e)->{
            draww.setVisible(true);
            transition.setRate(transition.getRate()*-1);
            transition.play();
            
            if(draww.isShown())
            {
                draww.close();
                draww.setVisible(false);
            }else{
                draww.open();
            }
        });
        //FIN DEL CODIGO PARA ABRIR EL MENU
    }    
    
    @FXML
    public void salir(){
        
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea salir?");
        if(ax == JOptionPane.YES_OPTION){
            System.exit(0);
            /*timerTask.cancel();
            timer.cancel();*/
        }

    }
    
    @FXML
    public void buscarId(){
        
        String aux=cedula.getText();
        ArrayList<String> datos=c.buscarEmpleado(aux);
        int tam=datos.size();
        if(tam>1){
            cedula.setDisable(true);
            nombre.setDisable(false);
            nombre.setText(datos.get(0));
            nombre.setDisable(true);
            sueldo.setDisable(false);
            cesta.setDisable(false);
            fecha_pago.setDisable(false);
            contrato.setDisable(false);
            fecha_ingreso.setDisable(false);
            cargo.setDisable(false);
            btn_aceptar.setDisable(false);
            btn_reset.setDisable(false);
        }else{
            JOptionPane.showMessageDialog(null, "EMPLEADO NO REGISTRADO.");
        }
        
    }
    
    @FXML
    public void registrar(){
        
        LocalDate ingreso=fecha_ingreso.getValue();
        DateTimeFormatter formater=DateTimeFormatter.ofPattern("dd-LLLL-yyyy");
        String ingresoAux=ingreso.format(formater);
        
        ArrayList<String> datos=new ArrayList<String>();
        datos.add(Integer.toString(c.generarId("rrhh")));
        datos.add(cedula.getText());
        datos.add(sueldo.getText());
        datos.add(cesta.getText());
        datos.add((String) fecha_pago.getValue());
        datos.add((String) contrato.getValue());
        datos.add(ingresoAux);
        datos.add(cargo.getText());
        
        datos.add(ivss.getText());
        datos.add(lph.getText());
        datos.add(pf.getText());
        
        int tam=datos.size();
        
        if(tam>6){
            c.ingresarRrhh(datos);
            JOptionPane.showMessageDialog(null, "INGRESO EXITOSO.");
            
            nombre.setText("");
            nombre.setDisable(true);
            sueldo.setText("");
            sueldo.setDisable(true);
            cesta.setText("");
            cesta.setDisable(true);
            fecha_pago.setDisable(true);
            contrato.setDisable(true);
            fecha_ingreso.setDisable(true);
            cargo.setText("");
            cargo.setDisable(true);
            btn_aceptar.setDisable(true);
            cedula.setText("");
            cedula.setDisable(false);
            cedula.requestFocus();
            btn_reset.setDisable(true);
            
        }else{
            JOptionPane.showMessageDialog(null, "POR FAVOR LLENE TODOS LOS CAMPOS.");
        }
    }
    
    @FXML
    public void leyes(){
        
        String s=sueldo.getText();
        int sueldo_basico=0;
        int comp=s.compareTo("");
        if(comp!=0){
            sueldo_basico=Integer.parseInt(s); 
        }
        double riesgo=0.9;
        double sso=0.4; //Retencion
        int lunes=4;
        double aporte_patronos=0.01;
        double trabajador=0.005;
        
        //IVSS
        double a=(sueldo_basico*12)/52;
        double b=a*sso;
        int c=(int) (b*lunes);
        ivss.setText(Integer.toString(c));
        
        //LPH
        int d=(int) ((sueldo_basico*aporte_patronos)/2);
        lph.setText(Integer.toString(d));
        
        //PARO FORZOSO
        double e=a*trabajador;
        int f=(int) (e*lunes);
        pf.setText(Integer.toString(f));
        
    }
    
    @FXML
    public void reset(){
        
        nombre.setText("");
        nombre.setDisable(true);
        sueldo.setText("");
        sueldo.setDisable(true);
        cesta.setText("");
        cesta.setDisable(true);
        fecha_pago.setDisable(true);
        contrato.setDisable(true);
        fecha_ingreso.setDisable(true);
        cargo.setText("");
        cargo.setDisable(true);
        btn_aceptar.setDisable(true);
        cedula.setText("");
        cedula.setDisable(false);
        cedula.requestFocus();
        btn_reset.setDisable(true);
        
    }
    
}
