package Vista;

import Controlador.Inventario;
import Controlador.Registro_Clientes;
import Controlador.Registro_Empleados;
import Controlador.Vender;
import Modelo.Conexion;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;


public class VentasController implements Initializable {

    @FXML
    private AnchorPane ventana_ventas;
    @FXML
    private JFXHamburger menu;
    @FXML
    private TableView<Vender> tabla_articulos;
    @FXML
    private TableColumn<Vender, String> id;
    @FXML
    private TableColumn<Vender, String> cod_interno;
    @FXML
    private TableColumn<Vender, String> descripcion;
    @FXML
    private TableColumn<Vender, String> precio;
    @FXML
    private TableColumn<Vender, String> cant;
    @FXML
    private TableColumn<Vender, String> total;
    @FXML
    private Label total_pagar;
    @FXML
    private Label nombre_cliente;
    @FXML
    private Label cedula_cliente;
    @FXML
    private JFXTextField cedula_entrada;
    @FXML
    private JFXTextField codigo;
    @FXML
    private JFXButton sumar;
    @FXML
    private JFXButton quitar;
    @FXML
    private JFXButton eliminar;
    @FXML
    private JFXButton vender;
    @FXML
    private JFXDrawer draww;
    @FXML
    private JFXButton btn_salir;
    
    final ObservableList<Vender> data = FXCollections.observableArrayList(); 

    Funciones f= new Funciones();
    Conexion c= new Conexion();
    
    static double iva=0.12;
    
    Date date = new Date();
    int año=date.getYear()+1900;
    int mes=date.getMonth()+1;
    int dia=date.getDate();
    String fecha=año+"-"+mes+"-"+dia;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        cedula_entrada.requestFocus();
        //-------Editar columna
        //cant.setCellFactory(TextFieldTableCell.<Vender>forTableColumn());
        //-------
        
        
        //Para insertar imagen en los botones
        Image image2 = new Image(getClass().getResourceAsStream("icon\\agregar.png"));
        sumar.setGraphic(new ImageView(image2));
        Image image4 = new Image(getClass().getResourceAsStream("icon\\eliminar.png"));
        quitar.setGraphic(new ImageView(image4));
        Image image3 = new Image(getClass().getResourceAsStream("icon\\eliminar_todo.png"));
        eliminar.setGraphic(new ImageView(image3));
        Image image1 = new Image(getClass().getResourceAsStream("icon\\vender.png"));
        vender.setGraphic(new ImageView(image1));
        Image image5 = new Image(getClass().getResourceAsStream("icon\\salir.png"));
        btn_salir.setGraphic(new ImageView(image5));
        
        total_pagar.setText("0");
        cedula_entrada.setStyle("-fx-prompt-text-fill: #424242;"); 
        codigo.setStyle("-fx-prompt-text-fill: #424242;"); 
        
        //------------
        draww.setVisible(false);
        
         //------CODIGO PARA ABRIR EL MENU
        try {
            AnchorPane box = FXMLLoader.load(getClass().getResource("subpanel.fxml"));
            draww.setSidePane(box);
        } catch (IOException ex) {
            Logger.getLogger(PanelController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        HamburgerBackArrowBasicTransition transition = new HamburgerBackArrowBasicTransition(menu);
        transition.setRate(-1);
        menu.addEventHandler(MouseEvent.MOUSE_PRESSED,(e)->{
            draww.setVisible(true);
            transition.setRate(transition.getRate()*-1);
            transition.play();
            
            if(draww.isShown())
            {
                draww.close();
                draww.setVisible(false);
            }else{
                draww.open();
            }
        });
        //FIN DEL CODIGO PARA ABRIR EL MENU
        
        //TIEMPO PARA ACTUALIZAR TABLA
        
        Timer timer2 = new Timer();
        TimerTask timerTask2;
        
        timerTask2 = new TimerTask()
        {
            public void run()
            {
                finalVenta();
            }
        };
        timer2.scheduleAtFixedRate(timerTask2, 0, 1000);

        //----
    }    

    @FXML
    public void insetarCliente() throws IOException {
        
        nombre_cliente.setText("");
        cedula_cliente.setText("");
        
        String ced=cedula_entrada.getText();
        ArrayList<String> cliente=c.buscarCliente(ced);
        
        int op=cliente.size();
        
        if(op>1){
            nombre_cliente.setText(cliente.get(0));
            cedula_cliente.setText(cliente.get(1));
            codigo.requestFocus();
        }else{
            JOptionPane.showMessageDialog(null, "Cliente no encontrado");
            int ax = JOptionPane.showConfirmDialog(null, "¿Desea registralo?");
            if(ax == JOptionPane.YES_OPTION){
                f.abrirSolamente("registrar_agregar.fxml");
            }
        }
        
    }
    
    @FXML
    public void insertarArticulo(){
        
        String cod=codigo.getText();
        ArrayList<String> articulo=c.buscarArticulo(cod);
        
        int op=articulo.size();
        
        if(op>1){
            
            id.setCellValueFactory(new PropertyValueFactory<Vender, String>("id"));
            cod_interno.setCellValueFactory(new PropertyValueFactory<Vender, String>("cod_interno"));
            descripcion.setCellValueFactory(new PropertyValueFactory<Vender, String>("descripcion"));
            precio.setCellValueFactory(new PropertyValueFactory<Vender, String>("precio"));
            cant.setCellValueFactory(new PropertyValueFactory<Vender, String>("cantidad"));
            total.setCellValueFactory(new PropertyValueFactory<Vender, String>("total"));
        
            tabla_articulos.setItems(data);
            
            String ax = JOptionPane.showInputDialog("Cantidad a llevar --> ");
            int aix=Integer.parseInt(ax);
            double sumatoria=aix * Double.parseDouble(articulo.get(3));
            String aux=Double.toString(sumatoria);
            
            data.add(new Vender(Integer.toString(contarId()), articulo.get(1), articulo.get(2), articulo.get(3), ax, aux));

            sumatoria();
            codigo.setText("");
            codigo.requestFocus();
        }else{
            JOptionPane.showMessageDialog(null, "Articulo no encontrado");
            codigo.setText("");
            codigo.requestFocus();
        }
        
    }

    @FXML
    public void quitar(){
        
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea eliminar el articulo?");
        if(ax == JOptionPane.YES_OPTION){
            try {
            Vender pos = tabla_articulos.getSelectionModel().getSelectedItem();
            tabla_articulos.getItems().remove(pos);
            sumatoria();
        } catch ( Exception e ) {
            JOptionPane.showMessageDialog(null, "Por favor seleccione una fila..");
            }
        } 
    }
    
    @FXML
    public void sumatoria(){
        double acum=0;
        for(int i=0; i<data.size();i++){
            String dato=(String) total.getCellObservableValue(i).getValue();
            acum=acum+Double.parseDouble(dato);
        }
        mostrarTotalPagar(acum);
    }
    
    @FXML
    public double subtotal(){
        double acum=0;
        for(int i=0; i<data.size();i++){
            String dato=(String) total.getCellObservableValue(i).getValue();
            acum=acum+Double.parseDouble(dato);
        }
        return acum;
    }
    
    @FXML
    public double total(){
        double acum=subtotal();
        double a=acum + (acum*iva);
        return a;
    }
    
    @FXML
    public void mostrarTotalPagar(double total_suma){
        
        double a=total_suma + (total_suma*iva);
        total_pagar.setText(Double.toString(a));
    }
    
    @FXML
    public int contarId(){
        int acum=0;
        for(int i=0; i<data.size();i++){
            String dato=(String) id.getCellObservableValue(i).getValue();
            acum=Integer.parseInt(dato);
        }
        return acum+1;
    }
    
    @FXML
    public void borrarTodo(){
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea borrar todo?");
        if(ax == JOptionPane.YES_OPTION){
            for ( int i = 0; i<tabla_articulos.getItems().size(); i++) {
                tabla_articulos.getItems().clear();
            }
            nombre_cliente.setText("");
            cedula_cliente.setText("");
            cedula_entrada.setText("");
            sumatoria();
            cedula_entrada.requestFocus();
        }  
    }
    
    @FXML
    public void registrarVenta() throws IOException{
        
        ArrayList<String> aux=new ArrayList<String>();
        int id_aux=c.generarId("ventas");
        aux.add(Integer.toString(id_aux));
        String comprobante=Integer.toString(año)+Integer.toString(mes)+Integer.toString(dia)+Integer.toString(id_aux);
        aux.add(comprobante);
        aux.add(fecha);
        aux.add(nombre_cliente.getText());
        aux.add(cedula_cliente.getText());
        aux.add(Integer.toString(data.size()));
        int aux_iva=(int) (iva*100);
        aux.add(Double.toString(subtotal()));
        aux.add(Integer.toString(aux_iva)+"%");
        aux.add(Double.toString(total()));
        aux.add("N");
        
        /*for (int i=0; i<aux.size();i++){
            System.out.println(aux.get(i));
        }*/
        c.ingresarVentas(aux);
        
        for(int i=0; i<data.size();i++){
            String a=(String) id.getCellObservableValue(i).getValue();
            String b=(String) cod_interno.getCellObservableValue(i).getValue();
            String cc=(String) descripcion.getCellObservableValue(i).getValue();
            String d=(String) precio.getCellObservableValue(i).getValue();
            String e=(String) cant.getCellObservableValue(i).getValue();
            String ff=(String) total.getCellObservableValue(i).getValue();
            ArrayList<String> abc=new ArrayList<String>();
            abc.add(Integer.toString(id_aux));
            abc.add(a);
            abc.add(b);
            abc.add(cc);
            abc.add(d);
            abc.add(e);
            abc.add(ff);
            c.ingresarVentasArticulos(abc);
            
            
            
        }
        
        f.abrirSolamente("facturacion.fxml");
    }
    
    @FXML
    public void salir(){
        
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea salir?");
        if(ax == JOptionPane.YES_OPTION){
            System.exit(0);
            /*timerTask.cancel();
            timer.cancel();*/
        }

    }
    
    @FXML
    public void finalVenta(){
        
        int estado=f.getEstado();
        if(estado==1){
            Funciones funciones=new Funciones(0);
            for ( int i = 0; i<tabla_articulos.getItems().size(); i++) {
                tabla_articulos.getItems().clear();
            }
            cedula_entrada.setText("");
        }
        
    }
    
}
