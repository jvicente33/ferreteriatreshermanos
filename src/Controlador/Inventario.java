
package Controlador;

import javafx.beans.property.SimpleStringProperty;


public class Inventario {
    
    private final SimpleStringProperty id;
    private final SimpleStringProperty descripcion; 
    private final SimpleStringProperty grupo; 
    private final SimpleStringProperty proveedor; 
    private final SimpleStringProperty cantidad;
    private final SimpleStringProperty precio;
    
    public Inventario(String idd, String descripcionn, String grupoo, String proveedorr, String cant, String precioo){
        
        this.id=new SimpleStringProperty(idd);
        this.descripcion=new SimpleStringProperty(descripcionn);
        this.grupo=new SimpleStringProperty(grupoo);
        this.proveedor=new SimpleStringProperty(proveedorr);
        this.cantidad=new SimpleStringProperty(cant);
        this.precio=new SimpleStringProperty(precioo);
 
    }
    
    public String getId(){
        return id.get();
    }
    public void setId(String idd){
        id.set(idd);
    }
    
    public String getDescripcion(){
        return descripcion.get();
    }
    public void setDescripcion(String descripcionn){
        descripcion.set(descripcionn);
    }
    
    public String getGrupo(){
        return grupo.get();
    }
    public void setGrupo(String grupoo){
        grupo.set(grupoo);
    }
    
    public String getProveedor(){
        return proveedor.get();
    }
    public void setProveedor(String proveedorr){
        proveedor.set(proveedorr);
    }
    
    public String getCantidad(){
        return cantidad.get();
    }
    public void setCantidad(String cant){
        cantidad.set(cant);
    }
    
    public String getPrecio(){
        return precio.get();
    }
    public void setPrecio(String precioo){
        precio.set(precioo);
    }
    
    

    
    
}
