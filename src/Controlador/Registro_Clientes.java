
package Controlador;

import javafx.beans.property.SimpleStringProperty;


public class Registro_Clientes {
    
    private final SimpleStringProperty id;
    private final SimpleStringProperty nombre; 
    private final SimpleStringProperty cedula; 
    private final SimpleStringProperty telefono_local; 
    private final SimpleStringProperty telefono_celular;
    private final SimpleStringProperty correo;
    
    public Registro_Clientes(String idd, String nombree, String cedulaa, String telf1, String telf2, String correoo){
        
        this.id=new SimpleStringProperty(idd);
        this.nombre=new SimpleStringProperty(nombree);
        this.cedula=new SimpleStringProperty(cedulaa);
        this.telefono_local=new SimpleStringProperty(telf1);
        this.telefono_celular=new SimpleStringProperty(telf2);
        this.correo=new SimpleStringProperty(correoo);
        
    }
    
    public String getId(){
        return id.get();
    }
    public String getNombre(){
        return nombre.get();
    }
    public String getCedula(){
        return cedula.get();
    }
    public String getTelefono(){
        return telefono_local.get();
    }
    public String getCelular(){
        return telefono_celular.get();
    }
    public String getCorreo(){
        return correo.get();
    }
    
    public void setId(String dato){
        id.set(dato);
    }
    public void setNombre(String dato){
        nombre.set(dato);
    }
    public void setCedula(String dato){
        cedula.set(dato);
    }
    public void setTelfono(String dato){
        telefono_local.set(dato);
    }
    public void setCelular(String dato){
        telefono_celular.set(dato);
    }
    public void setEmail(String dato){
        correo.set(dato);
    }
    
    
}
